from sqlalchemy import Column, String
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class HistoricalWeatherData(Base):
    __tablename__ = 'owm_historical_data'

    query = Column(String(100), primary_key=True)
    answer = Column(String)
