import json
from dynaconf import settings
from sqlalchemy import select
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker

from main.models import (
    GetHistoricalWeatherRequest,
    HistoricalWeather,
)
from main.orm import HistoricalWeatherData


engine = create_async_engine(settings.DB_DSN, echo=True)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)


async def get_weather_from_db(weather_request: GetHistoricalWeatherRequest):
    async with async_session() as session:
        result = await session.execute(
            select(HistoricalWeatherData.answer).where(HistoricalWeatherData.query == weather_request.query)
        )
    data = result.one_or_none()
    if data is not None:
        return HistoricalWeather.parse_obj(json.loads(data.answer))


async def put_weather_in_db(weather_request: GetHistoricalWeatherRequest, weather_data: HistoricalWeather):
    async with async_session() as session:
        result = await session.execute(
            select(HistoricalWeatherData).where(HistoricalWeatherData.query == weather_request.query).exist()
        )
        if not result:
            session.add(HistoricalWeatherData(
                query=weather_request.query, answer=weather_data.json(by_alias=True, exclude_none=True)
            ))
            session.commit()
