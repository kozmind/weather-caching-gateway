from main.models import HistoricalWeather, GetHistoricalWeatherRequest
from main.db import get_weather_from_db, put_weather_in_db
from main.data_provider import get_weather_from_provider


async def get_weather(weather_request: GetHistoricalWeatherRequest) -> HistoricalWeather:
    weather_data = await get_weather_from_db(weather_request)
    if weather_data is None:
        weather_data = await get_weather_from_provider(weather_request)
        if weather_data is not None:
            await put_weather_in_db(weather_request, weather_data)
    return weather_data
