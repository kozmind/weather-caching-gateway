from httpx import AsyncClient
from dynaconf import settings
from main.logger import logger
from main.models import (
    HistoricalWeather,
    GetHistoricalWeatherRequest,
)


async def _request_get(url):
    async with AsyncClient() as client:
        return await client.get(url)


def _build_log_data(url, resp):
    return {
        'type': 'request to data provider',
        'request': {
            'url': url
        },
        'response': {
            'status_code': resp.status_code,
            'text': resp.json()
        }
    }


async def get_weather_from_provider(wr: GetHistoricalWeatherRequest):
    url = f'{settings.OPENWEATHERMAP_GET_HISTORICAL_WEATHER_ENDPOINT}?' \
          f'q={wr.city},{wr.country_code}&type=hour&start={wr.date.timestamp()}&cnt=1&' \
          f'apikey={settings.OPENWEATHERMAP_APIKEY}'
    response = await _request_get(url)

    if response.status_code == 200:
        logger.info(_build_log_data(url, response))
        data = response.json().get('list', None)
        if isinstance(data, list) and len(data) > 0:
            return HistoricalWeather.parse_obj(data[0])
    else:
        logger.error(_build_log_data(url, response))
