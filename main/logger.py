import sys
from loguru import logger

logger.remove()
logger.add(
    sys.stderr,
    colorize=True,
    format="<blue>{level:10}</blue><green>{time:%m/%d/%Y %H:%M:%S}</green> - {message}",
    level="DEBUG",
)
