from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator, Field

API_DATETIME_FORMAT = "%d.%m.%YT%H:%M"


class GetHistoricalWeatherRequest(BaseModel):
    country_code: str
    city: str
    date: datetime

    @validator('date', pre=True)
    def str_to_date(cls, v):
        return datetime.strptime(v, API_DATETIME_FORMAT)

    @property
    def query(self):
        return f'country_code={self.country_code}&city={self.city}&date={self.date.strftime(API_DATETIME_FORMAT)}'


class MainWeatherData(BaseModel):
    temp: float
    pressure: float
    humidity: int
    temp_min: float
    temp_max: float
    sea_level: float
    grnd_level: float


class WindData(BaseModel):
    speed: float
    deg: float


class CloudsData(BaseModel):
    all: int


class RainData(BaseModel):
    one_hour: Optional[float] = Field(alias='1h')
    three_hours: Optional[float] = Field(alias='3h')


class SnowData(BaseModel):
    one_hour: Optional[float] = Field(alias='1h')
    three_hours: Optional[float] = Field(alias='3h')


class HistoricalWeather(BaseModel):
    main: MainWeatherData
    wind: WindData
    clouds: CloudsData
    rain: Optional[RainData] = None
    snow: Optional[SnowData] = None
