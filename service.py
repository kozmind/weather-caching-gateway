from fastapi import FastAPI, HTTPException
from main.models import (
    GetHistoricalWeatherRequest,
    HistoricalWeather,
)
from main.core import get_weather

tags_metadata = [
    {
        'name': 'health',
        'description': 'Возвращает данные о работоспособности сервиса'
    },
    {
        "name": "historical_weather",
        "description": "Возвращает исторические данные о погоде в определенное время в определенном городе",
    },
]

app = FastAPI(openapi_tags=tags_metadata)


@app.get("/health", tags=['health'])
async def get_health_api():
    return {'status': 'ok'}


@app.get(
    "/weather",
    tags=['historical_weather'],
    response_model=HistoricalWeather,
    response_model_exclude_none=True,
    response_model_by_alias=True
)
async def get_weather_api(country_code: str, city: str, date: str):
    wr = GetHistoricalWeatherRequest(country_code=country_code, city=city, date=date)
    res = await get_weather(wr)
    if res is None:
        raise HTTPException(404)
    else:
        return res
