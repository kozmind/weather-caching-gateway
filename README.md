# Weather caching gateway
### Версия: 0.1.0
Кеширующий шлюз данных о погоде.  
Является шлюзом к API OpenWeatherMap. Все запросы и ответы сервера кешируются, 
для исключения повторных длительных запросов.

## Пример использования
```zsh
> curl 'http://127.0.0.1:8000/weather?country_code=RU&city=Moscow&date=20.05.2021T12:00'
{"main":{"temp":266.052,"pressure":957.86,"humidity":90,"temp_min":266.052,"temp_max":266.052,"sea_level":1039.34,"grnd_level":957.86},"wind":{"speed":1.16,"deg":139.502},"clouds":{"all":0},"rain":{"1h":0.35}}
```

## Язык реализации
python 3.8

## Зависимости
Управляются poetry. Команда создаст виртуальное окружение и установит зависимости.
```shell
poetry install
```
Для установки без dev-зависимостей используйте флаг `--no-dev`

## Конфигурирование
1. Переименуйте файл `.secrets.yaml.tmpl` в `.secrets.yaml`. В нем хранятся секретные данные (пароли, ключи к API и т.д.). Заполните его своими данными.
2. В файле `settings.yaml` хранятся открытые настройки. Скорее всего, вам не нужно в нем что-либо изменять.
3. В этих файлах вы можете делать настройки сразу для нескольких окружений (например, `development`, `staging`, `testing`, `production` или `default`).
4. Окружение по умолчанию `development`. Используйте переменную окружения `ENV_FOR_DYNACONF` для его изменения.
5. Тесты запускаются в окружении `testing`.

### Миграции БД
Для управления миграциями используется `alembic`. Настройки для различных окружений такие же, как и для сервиса.
Накатить миграции до последней c окружением по умолчанию:
```shell
alembic upgrade head
```
То же самое, но c конкретным окружением:
```shell
env ENV_FOR_DYNACONF=staging alembic upgrade head
```

## Запуск
```shell
python service.py
```

## Документация API
Swagger-документация доступна по ручке `/docs`  
ReDoc-документация доступна по ручке `/redoc`

## Тестирование
Установить dev-зависимости.  
Из корневого каталога проекта запустить:
```shell
python -m pytest -vv
```
По умолчанию интеграционные тесты пропускаются. Чтобы не пропускать добавьте аргумент `--integration`.
```shell
python -m pytest -vv --integration
```

Проверить покрытие тестами:
```shell
python -m pytest --cov=main
```


## Развертывание в Docker
Из корневого каталога проекта запустить:
```shell
docker-compose up
```

## Автор
Дмитрий Козьмин <7regtw@gmail.com>