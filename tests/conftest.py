from dynaconf import settings
import pytest
from loguru import logger
from main.models import GetHistoricalWeatherRequest, HistoricalWeather


def pytest_addoption(parser):
    # Добавляем атрибут запуска pytest
    parser.addoption(
        '--integration',
        action="store_true",
        default=False,
        help="запускает интеграционные тесты"
    )


def pytest_runtest_setup(item):
    # Пропускаем все тесты с маркером "integration"
    if not item.config.getoption('--integration'):
        for marker in item.iter_markers():
            logger.debug(marker.name)
            if 'integration' == marker.name:
                pytest.skip('Integration tests are not run by default')


@pytest.fixture(scope="session", autouse=True)
def set_test_settings():
    # Тесты запускаются, выставляя окружение для настроек testing
    settings.configure(FORCE_ENV_FOR_DYNACONF="testing")


_weather_data = {
    "main": {
            "temp": 266.052,
            "pressure": 957.86,
            "humidity": 90,
            "temp_min": 266.052,
            "temp_max": 266.052,
            "sea_level": 1039.34,
            "grnd_level": 957.86,
    },
    "wind": {"speed": 1.16, "deg": 139.502},
    "clouds": {"all": 0},
    "rain": {"1h": 0.35}
}

_historical_weather = HistoricalWeather.parse_obj(_weather_data)

_weather_request_data = {
    'country_code': 'RU',
    'city': 'Moscow',
    'date': '20.05.2021T12:00'
}


def _weather_response_from_owm_api():
    class WeatherResponse:
        json_data = None

        def json(self):
            return self.json_data

    res = WeatherResponse()
    res.status_code = 200
    res.json_data = {'list': [_weather_data]}
    return res


@pytest.fixture
def weather_data():
    return _weather_data


@pytest.fixture
def weather_request_data():
    return _weather_request_data


@pytest.fixture
def weather_request_obj():
    return GetHistoricalWeatherRequest.parse_obj(_weather_request_data)


@pytest.fixture
def weather_response_from_owm_api():
    return _weather_response_from_owm_api()


@pytest.fixture
def historical_weather():
    return _historical_weather


@pytest.fixture
def base_url():
    # TODO чтоб бралось из настроек
    return "http://localhost:8000"


@pytest.fixture
def get_historical_weather_request():
    return GetHistoricalWeatherRequest.parse_obj(_weather_request_data)


@pytest.fixture
def mocks():
    class Mocks:
        @staticmethod
        async def mock_return_none(*args):
            return None

        @staticmethod
        async def mock_return_historical_weather(*args):
            return _historical_weather

        @staticmethod
        async def mock_request_get_from_owm_api(*args):
            return _weather_response_from_owm_api()

    return Mocks



