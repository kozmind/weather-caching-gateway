import pytest
import main.core as core


@pytest.mark.anyio
async def test_get_weather_data_not_found(monkeypatch, weather_request_data, get_historical_weather_request, mocks):

    monkeypatch.setattr(core, 'get_weather_from_db', mocks.mock_return_none)
    monkeypatch.setattr(core, 'get_weather_from_provider', mocks.mock_return_none)

    res = await core.get_weather(get_historical_weather_request)

    assert res is None


@pytest.mark.anyio
async def test_get_weather_data_found_from_provider(
        monkeypatch, weather_data, weather_request_data, historical_weather, get_historical_weather_request, mocks):

    monkeypatch.setattr(core, 'get_weather_from_db', mocks.mock_return_none)
    monkeypatch.setattr(core, 'put_weather_in_db', mocks.mock_return_none)
    monkeypatch.setattr(core, 'get_weather_from_provider', mocks.mock_return_historical_weather)

    res = await core.get_weather(get_historical_weather_request)

    assert res == historical_weather


@pytest.mark.anyio
async def test_get_weather_data_found_from_db(
        monkeypatch, weather_data, weather_request_data, historical_weather, get_historical_weather_request, mocks):

    monkeypatch.setattr(core, 'get_weather_from_db', mocks.mock_return_historical_weather)
    monkeypatch.setattr(core, 'get_weather_from_provider', mocks.mock_return_none)

    res = await core.get_weather(get_historical_weather_request)

    assert res == historical_weather
