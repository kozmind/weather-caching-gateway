import pytest
from httpx import AsyncClient
import service


async def _request_get_app(base_url, params):
    async with AsyncClient(app=service.app, base_url=base_url) as ac:
        return await ac.get("/weather", params=params)


@pytest.mark.anyio
async def test_weather_handler_data_not_found(monkeypatch, base_url, weather_request_data, mocks):

    monkeypatch.setattr(service, 'get_weather', mocks.mock_return_none)

    response = await _request_get_app(base_url, weather_request_data)

    assert response.status_code == 404
    assert response.json() == {'detail': 'Not Found'}


@pytest.mark.anyio
async def test_weather_handler_data_is_found(monkeypatch, base_url, weather_request_data, weather_data, mocks):

    monkeypatch.setattr(service, 'get_weather', mocks.mock_return_historical_weather)

    response = await _request_get_app(base_url, weather_request_data)

    assert response.status_code == 200
    assert response.json() == weather_data


@pytest.mark.anyio
async def test_weather_handler_request_is_wrong(monkeypatch, base_url):

    bad_weather_request = {}
    response = await _request_get_app(base_url, bad_weather_request)

    assert response.status_code == 422
    assert response.json() == {
        'detail': [
            {'loc': ['query', 'country_code'], 'msg': 'field required', 'type': 'value_error.missing'},
            {'loc': ['query', 'city'], 'msg': 'field required', 'type': 'value_error.missing'},
            {'loc': ['query', 'date'], 'msg': 'field required', 'type': 'value_error.missing'},
        ]
    }
