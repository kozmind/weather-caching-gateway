import pytest
import main.data_provider
from main.models import HistoricalWeather
from main.data_provider import get_weather_from_provider


@pytest.mark.integration
@pytest.mark.third_party_api
@pytest.mark.anyio
async def test_get_weather_from_provider_positive(weather_request_obj):

    res = await get_weather_from_provider(weather_request_obj)

    assert isinstance(res, HistoricalWeather)


@pytest.mark.third_party_api
@pytest.mark.anyio
async def test_get_weather_from_provider_with_monkeypatch_positive(
        monkeypatch, weather_request_obj, weather_data, weather_response_from_owm_api, mocks):

    monkeypatch.setattr(main.data_provider, '_request_get', mocks.mock_request_get_from_owm_api)

    res = await get_weather_from_provider(weather_request_obj)

    assert isinstance(res, HistoricalWeather)
